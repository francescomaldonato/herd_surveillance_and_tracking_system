
//SHEEP MAP JAVASCRIPT

var reconnectTimeout = 2000;
var host="mqtt.flespi.io"; 
var port=443;
var token = "9qV3kCOJmoHY5dtvsbRtg0EiCIVlFNgE10xbs9KClhrfiKABJhSP6mXySJHnkMQb";
var subscribeTopic = "response";
var liste ="";

var marker_line2 = [];
var marker_line3 = [];
var marker_line4 = [];
var marker_line5 = [];
var marker_line6 = [];

var history2 = [];
var history3 = [];
var history4 = [];
var history5 = [];
var history6 = [];

function getdate_to(){
    window.addEventListener('load', () => {
        const now = new Date();
        now.setMinutes(now.getMinutes());
        document.getElementById('time_to').value = now.toISOString().slice(0, 16);
      });
}
function getdate_from(){
    window.addEventListener('load', () => {
        const now = new Date();
        now.setMonth(now.getMonth()-2);
        now.setMinutes(now.getMinutes());
        document.getElementById('time_from').value = now.toISOString().slice(0, 16);
      });
}

function show_live(){
    /* var pagebutton= document.getElementById("selfclick");
    pagebutton.click(); */

    sqlite_q =  "SELECT * FROM data GROUP BY SOURCE HAVING MAX(TIME);";
    var requete = { "message":"Connection request form Web page","data": sqlite_q};

    message = new Paho.MQTT.Message(JSON.stringify(requete));
    message.destinationName = "query";
    client.send(message);
}



var client = new Paho.MQTT.Client(host, Number(port), "webpage");

// set callback handlers
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;
var options = {
    timeout: 10,
    onSuccess: onConnect,
    onFailure: onFailure,
    userName : token,
    password : '',
    useSSL: true,
    cleanSession : true
};

client.connect(options);


//function for loading the database of positions and polylines
function loadhistory(){

    var MAP_ZOOM = 14;//14
    //var MARKER_SIZE = 24;
    var center= {lat: 43.6239695, lng: 7.0509146}

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: MAP_ZOOM,
        //center: center,
        //mapTypeId: 'satellite'
        mapTypeId: 'terrain'
        });

    var bounds = new google.maps.LatLngBounds();
    var infowindow = new google.maps.InfoWindow();
    
        //Source 1 GATEWAY Position
    
        var icon_source1 = {
            url: "https://231webdev.com/wp-content/uploads/2015/05/RaspberryPi-logo.png",// url
            scaledSize: new google.maps.Size(40, 40), // scaled size
            origin: new google.maps.Point(0,0), // origin
        };
        k=1;
        /* var gateway = {lat:43.615349, lng:7.072597}; */
        var gateway = {lat:44.100877, lng:6.952468};

        temp_marker1 = new google.maps.Marker({
            position: gateway,
            title: "source #1 Raspbpi GATEWAY",
            map: map,
            animation: google.maps.Animation.DROP,
            icon: icon_source1
        });

        var infowindow = new google.maps.InfoWindow({
            content: "source #1 Raspbpi GATEWAY"
            });

        google.maps.event.addListener(temp_marker1, 'mouseover', (function(temp_marker1, k) {
            return function() {
            infowindow.setContent("source #1 Raspbpi GATEWAY");
            infowindow.open(map, temp_marker1);
            }
            })(temp_marker1, k));

        google.maps.event.addListener(temp_marker1, 'click', (function(temp_marker1, k) {
            return function() {
            infowindow.setContent("source #1 Raspbpi GATEWAY" + "<br>" + "Position Lat: " + JSON.stringify(gateway.lat) + " Lon: " + JSON.stringify(gateway.lng));
            infowindow.open(map, temp_marker1);
            }
            })(temp_marker1, k));

        google.maps.event.addListener(temp_marker1, 'mouseout', (function(temp_marker1, k) {
            return function() {
            infowindow.close();
            }
            })(temp_marker1, k));

        //extend the bounds to include each marker's position
        bounds.extend(temp_marker1.position);
        //now fit the map to the newly inclusive bounds
        map.fitBounds(bounds);
        temp_marker1.setMap(map);
    
        //END of code for Source 1

    var flightPlanCoordinates2= [];
    var flightPlanCoordinates3= [];
    var flightPlanCoordinates4= [];
    var flightPlanCoordinates5= [];
    var flightPlanCoordinates6= [];

    //Source 2
    for(i=0; i<history2.length; i++){
        temp_history2 = history2[i];
        temp_marker2 = marker_line2[i];
        //constant check on position and displaying
        flightPlanCoordinates2.push({lat: temp_history2.Longitude, lng: temp_history2.Latitude});

        var icon_source2 = {
            url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png"// url
        };

        temp_marker2 = new google.maps.Marker({
            position: {lat: temp_history2.Longitude, lng: temp_history2.Latitude},
            title:"history of source #" + JSON.stringify(temp_history2.Source),
            map: map,
            animation: google.maps.Animation.DROP,
            icon: icon_source2
        });

        var infowindow = new google.maps.InfoWindow({
            content: "source #" + JSON.stringify(temp_history2.Source) + "<br>" + "at time:" + JSON.stringify(temp_history2.Time)
          });

        google.maps.event.addListener(temp_marker2, 'mouseover', (function(temp_marker2, i) {
            return function() {
            infowindow.setContent("source #" + JSON.stringify(temp_history2.Source) + "<br>" + "at time:" + JSON.stringify(temp_history2.Time));
            infowindow.open(map, temp_marker2);
            }
            })(temp_marker2, i));

        google.maps.event.addListener(temp_marker2, 'click', (function(temp_marker2, i) {
            return function() {
            infowindow.setContent("source #" + JSON.stringify(temp_history2.Source) + "<br>" + "at time:" + JSON.stringify(temp_history2.Time) + "<br>" + "Position Lat: " + JSON.stringify(temp_history2.Latitude) + " Lon: " + JSON.stringify(temp_history2.Longitude));
            infowindow.open(map, temp_marker2);
            }
            })(temp_marker2, i));

        google.maps.event.addListener(temp_marker2, 'mouseout', (function(temp_marker2, i) {
            return function() {
            infowindow.close();
            }
            })(temp_marker2, i));

        //extend the bounds to include each marker's position
        bounds.extend(temp_marker2.position);
        //now fit the map to the newly inclusive bounds
        map.fitBounds(bounds);
    }

    var flightPath2 = new google.maps.Polyline({
    path: flightPlanCoordinates2,
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 0.5,
    strokeWeight: 15
    });

    //map.addOverlay(flightPath2);
    flightPath2.setMap(map);

    //END of code for Source 2


    //Source 3
    for(i=0; i<history3.length; i++){
        temp_history3 = history3[i];
        temp_marker3 = marker_line3[i];
        //constant check on position and displaying
        flightPlanCoordinates3.push({lat: temp_history3.Longitude, lng: temp_history3.Latitude});

        

        var icon_source3 = {
            url: "http://maps.google.com/mapfiles/ms/icons/green-dot.png", // url
            //url: 'https://maps.google.com/mapfiles/kml/shapes/',
            //scaledSize: new google.maps.Size(25, 25*1.62), // scaled size
            //origin: new google.maps.Point(0,0), // origin
            //anchor: new google.maps.Point(0, 0) // anchor
            //path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
            //strokeColor: "green",
            //scale: 3
        };

        temp_marker3 = new google.maps.Marker({
            position: {lat: temp_history3.Longitude, lng: temp_history3.Latitude},
            title:"history of source #" + JSON.stringify(temp_history3.Source),
            map: map,
            animation: google.maps.Animation.DROP,
            icon: icon_source3
        });

        var infowindow = new google.maps.InfoWindow({
            content: "source #" + JSON.stringify(temp_history3.Source) + "<br>" + "at time:" + JSON.stringify(temp_history3.Time)
          });

        

        google.maps.event.addListener(temp_marker3, 'mouseover', (function(temp_marker3, i) {
            return function() {
            infowindow.setContent("source #" + JSON.stringify(temp_history3.Source) + "<br>" + "at time:" + JSON.stringify(temp_history3.Time));
            infowindow.open(map, temp_marker3);
            }
            })(temp_marker3, i));
        
        

        google.maps.event.addListener(temp_marker3, 'click', (function(temp_marker3, i) {
            return function() {
            infowindow.setContent("source #" + JSON.stringify(temp_history3.Source) + "<br>" + "at time:" + JSON.stringify(temp_history3.Time) + "<br>" + "Position Lat: " + JSON.stringify(temp_history3.Latitude) + " Lon: " + JSON.stringify(temp_history3.Longitude));
            infowindow.open(map, temp_marker3);
            }
            })(temp_marker3, i));

        google.maps.event.addListener(temp_marker3, 'mouseout', (function(temp_marker3, i) {
            return function() {
            infowindow.close();
            }
            })(temp_marker3, i));

        //extend the bounds to include each marker's position
        bounds.extend(temp_marker3.position);
        //now fit the map to the newly inclusive bounds
        map.fitBounds(bounds);
    }

    var flightPath3 = new google.maps.Polyline({
    path: flightPlanCoordinates3,
    geodesic: true,
    strokeColor: '#00FF00',
    strokeOpacity: 0.5,
    strokeWeight: 15
    });

    //map.addOverlay(flightPath2);
    flightPath3.setMap(map);
    //END of code for Source 3



    //Source 4
    for(i=0; i<history4.length; i++){
        temp_history4 = history4[i];
        temp_marker4 = marker_line4[i];
        //constant check on position and displaying
        flightPlanCoordinates4.push({lat: temp_history4.Longitude, lng: temp_history4.Latitude});

        var icon_source4 = {
            url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"// url
            //scaledSize: new google.maps.Size(20, 40), // scaled size
            //origin: new google.maps.Point(0,0), // origin
            //anchor: new google.maps.Point(0, 0) // anchor
        };

        temp_marker4 = new google.maps.Marker({
            position: {lat: temp_history4.Longitude, lng: temp_history4.Latitude},
            title:"history of source #" + JSON.stringify(temp_history4.Source),
            map: map,
            animation: google.maps.Animation.DROP,
            icon: icon_source4
        });

        var infowindow = new google.maps.InfoWindow({
            content: "source #" + JSON.stringify(temp_history4.Source) + "<br>" + "at time:" + JSON.stringify(temp_history4.Time)
          });

        google.maps.event.addListener(temp_marker4, 'mouseover', (function(temp_marker4, i) {
            return function() {
            infowindow.setContent("source #" + JSON.stringify(temp_history4.Source) + "<br>" + "at time:" + JSON.stringify(temp_history4.Time));
            infowindow.open(map, temp_marker4);
            }
            })(temp_marker4, i));

        google.maps.event.addListener(temp_marker4, 'click', (function(temp_marker4, i) {
            return function() {
            infowindow.setContent("source #" + JSON.stringify(temp_history4.Source) + "<br>" + "at time:" + JSON.stringify(temp_history4.Time) + "<br>" + "Position Lat: " + JSON.stringify(temp_history4.Latitude) + " Lon: " + JSON.stringify(temp_history4.Longitude));
            infowindow.open(map, temp_marker4);
            }
            })(temp_marker4, i));

        google.maps.event.addListener(temp_marker4, 'mouseout', (function(temp_marker4, i) {
            return function() {
            infowindow.close();
            }
            })(temp_marker4, i));

        //extend the bounds to include each marker's position
        bounds.extend(temp_marker4.position);
        //now fit the map to the newly inclusive bounds
        map.fitBounds(bounds);
    }

    var flightPath4 = new google.maps.Polyline({
    path: flightPlanCoordinates4,
    geodesic: true,
    strokeColor: '#0000FF',
    strokeOpacity: 0.5,
    strokeWeight: 15
    });

    //map.addOverlay(flightPath2);
    flightPath4.setMap(map);
    //END of code for Source 4


    //Source 5
    for(i=0; i<history5.length; i++){
        temp_history5 = history5[i];
        temp_marker5 = marker_line5[i];
        //constant check on position and displaying
        flightPlanCoordinates5.push({lat: temp_history5.Longitude, lng: temp_history5.Latitude});

        var icon_source5 = {
            url: "https://webstockreview.net/images/sheep-clipart-cow-14.png",// url
            scaledSize: new google.maps.Size(40, 40), // scaled size
            origin: new google.maps.Point(0,-10), // origin
            //anchor: new google.maps.Point(0, 0) // anchor
        };

        temp_marker5 = new google.maps.Marker({
            position: {lat: temp_history5.Longitude, lng: temp_history5.Latitude},
            title:"history of source #" + JSON.stringify(temp_history5.Source),
            map: map,
            animation: google.maps.Animation.DROP,
            icon: icon_source5
        });

        var infowindow = new google.maps.InfoWindow({
            content: "source #" + JSON.stringify(temp_history5.Source) + "<br>" + "at time:" + JSON.stringify(temp_history5.Time)
          });

        google.maps.event.addListener(temp_marker5, 'mouseover', (function(temp_marker5, i) {
            return function() {
            infowindow.setContent("source #" + JSON.stringify(temp_history5.Source) + "<br>" + "at time:" + JSON.stringify(temp_history5.Time));
            infowindow.open(map, temp_marker5);
            }
            })(temp_marker5, i));

        google.maps.event.addListener(temp_marker5, 'click', (function(temp_marker5, i) {
            return function() {
            infowindow.setContent("source #" + JSON.stringify(temp_history5.Source) + "<br>" + "at time:" + JSON.stringify(temp_history5.Time) + "<br>" + "Position Lat: " + JSON.stringify(temp_history5.Latitude) + " Lon: " + JSON.stringify(temp_history5.Longitude));
            infowindow.open(map, temp_marker5);
            }
            })(temp_marker5, i));

        google.maps.event.addListener(temp_marker5, 'mouseout', (function(temp_marker5, i) {
            return function() {
            infowindow.close();
            }
            })(temp_marker5, i));

        //extend the bounds to include each marker's position
        bounds.extend(temp_marker5.position);
        //now fit the map to the newly inclusive bounds
        map.fitBounds(bounds);
    }

    var flightPath5 = new google.maps.Polyline({
    path: flightPlanCoordinates5,
    geodesic: true,
    strokeColor: '#FFFF00',
    strokeOpacity: 0.5,
    strokeWeight: 15
    });

    //map.addOverlay(flightPath2);
    flightPath5.setMap(map);
    //END of code for Source 5


    //Source 6
    for(i=0; i<history6.length; i++){
        temp_history6 = history6[i];
        temp_marker6 = marker_line6[i];
        //constant check on position and displaying
        flightPlanCoordinates6.push({lat: temp_history6.Longitude, lng: temp_history6.Latitude});

        var icon_source6 = {
            url: "https://vignette.wikia.nocookie.net/scribblenauts/images/c/c1/German_Sheperd.png/revision/latest?cb=20130711185619",// url
            scaledSize: new google.maps.Size(40, 40), // scaled size
            origin: new google.maps.Point(0,-10), // origin
            //anchor: new google.maps.Point(0, 0) // anchor
        };

        temp_marker6 = new google.maps.Marker({
            position: {lat: temp_history6.Longitude, lng: temp_history6.Latitude},
            title:"history of source #" + JSON.stringify(temp_history6.Source),
            map: map,
            animation: google.maps.Animation.DROP,
            icon: icon_source6
        });

        var infowindow = new google.maps.InfoWindow({
            content: "source #" + JSON.stringify(temp_history6.Source) + "<br>" + "at time:" + JSON.stringify(temp_history6.Time)
          });

        google.maps.event.addListener(temp_marker6, 'mouseover', (function(temp_marker6, i) {
            return function() {
            infowindow.setContent("source #" + JSON.stringify(temp_history6.Source) + "<br>" + "at time:" + JSON.stringify(temp_history6.Time));
            infowindow.open(map, temp_marker6);
            }
            })(temp_marker6, i));

        google.maps.event.addListener(temp_marker6, 'click', (function(temp_marker6, i) {
            return function() {
            infowindow.setContent("source #" + JSON.stringify(temp_history6.Source) + "<br>" + "at time:" + JSON.stringify(temp_history6.Time) + "<br>" + "Position Lat: " + JSON.stringify(temp_history6.Latitude) + " Lon: " + JSON.stringify(temp_history6.Longitude));
            infowindow.open(map, temp_marker6);
            }
            })(temp_marker6, i));

        google.maps.event.addListener(temp_marker6, 'mouseout', (function(temp_marker6, i) {
            return function() {
            infowindow.close();
            }
            })(temp_marker6, i));

        //extend the bounds to include each marker's position
        bounds.extend(temp_marker6.position);
        //now fit the map to the newly inclusive bounds
        map.fitBounds(bounds);
    }

    var flightPath6 = new google.maps.Polyline({
    path: flightPlanCoordinates6,
    geodesic: true,
    strokeColor: '#FF00FF',
    strokeOpacity: 0.5,
    strokeWeight: 15
    });

    //map.addOverlay(flightPath2);
    flightPath6.setMap(map);
    //END of code for Source 6

}
 

// called when the client loses its connection
function onConnectionLost(responseObject) {
          if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:"+responseObject.errorMessage);
          }
}	 

function onFailure(message) {
    console.log("Connection Attempt to Host "+host+" Failed");
    console.log(message);
    setTimeout(MQTTconnect, reconnectTimeout);
}

function onMessageArrived(msg){
    out_msg="Message received "+msg.payloadString+"<br>";
    out_msg=out_msg+"Message received Topic "+msg.destinationName;
    $("#topic1").text(msg.destinationName);

/*     liste +="\n"+ msg.payloadString;
    $("#textarea1").text(liste); */


    /*
    for (let i = 0; i < marker_line.length; i++) {
        marker_line[i].setMap(null);
    }
    marker_line = [];
    */


    var Result = JSON.parse(msg.payloadString);

    if(Result.length>0){
        console.log(Result);

        if(Result[0].SOURCE <7 && Result[0].SOURCE >= 0){

            for (let i = 0; i < marker_line2.length; i++) {
                marker_line2[i].setMap(null);
            }
            marker_line2 = [];
            for (let i = 0; i < marker_line3.length; i++) {
                marker_line3[i].setMap(null);
            }
            marker_line3 = [];
            for (let i = 0; i < marker_line4.length; i++) {
                marker_line4[i].setMap(null);
            }
            marker_line4 = [];
            for (let i = 0; i < marker_line5.length; i++) {
                marker_line5[i].setMap(null);
            }
            marker_line5 = [];
            for (let i = 0; i < marker_line6.length; i++) {
                marker_line6[i].setMap(null);
            }
            marker_line6 = [];

            history2 = [];
            history3 = [];
            history4 = [];
            history5 = [];
            history6 = [];



            for(i=0; i<Result.length; i++){
                var temp_result = Result[i];
                var Longitude = temp_result.LONGITUDE;
                var Latitude = temp_result.LATITUDE;
                var Source = temp_result.SOURCE;
                var Time = temp_result.TIME;

                switch(Source) {
                    case 1:
                    // code block
                    break;
                    case 2:
                        var data ={"Longitude": Longitude, "Latitude": Latitude, "Source": Source, "Time": Time};
                        history2.push(data);
                        //console.log(Source);
                    break;
                    case 3:
                        var data ={"Longitude": Longitude, "Latitude": Latitude, "Source": Source, "Time": Time};
                        history3.push(data);
                    break;
                    case 4:
                        var data ={"Longitude": Longitude, "Latitude": Latitude, "Source": Source, "Time": Time};
                        history4.push(data);
                    break;
                    case 5:
                        var data ={"Longitude": Longitude, "Latitude": Latitude, "Source": Source, "Time": Time};
                        history5.push(data);
                    break;
                    case 6:
                        var data ={"Longitude": Longitude, "Latitude": Latitude, "Source": Source, "Time": Time};
                        history6.push(data);
                    break;
                    default:
                    // code block
                }
            }

            loadhistory();


        }

            
        
    }

}

function getFilters() {
    var from = document.getElementById("time_from").value;
    var to = document.getElementById("time_to").value;
    from_date = from.slice(0,10);
    from_hour = from.slice(11,17);
    to_date = to.slice(0,10);
    to_hour = to.slice(11,17);

    var source_2 = document.getElementById("source2").checked;
    var source_3 = document.getElementById("source3").checked;
    var source_4 = document.getElementById("source4").checked;
    var source_5 = document.getElementById("source5").checked;
    var source_6 = document.getElementById("source6").checked;

    var sqlite_q = "SELECT * FROM data  WHERE DATETIME(   substr(TIME,1,4)||'-'||   substr(TIME,6,2)||'-'||   substr(TIME,9,2)||' '||   substr(TIME,12,8) )  BETWEEN DATETIME('" + from_date + " " + from_hour + ":00') AND DATETIME('" + to_date + " " + to_hour + ":00') AND (";
    /* var sqlite_q = "SELECT * FROM data WHERE "; */

    if(source_2){sqlite_q = sqlite_q + " SOURCE = 2 ";}
    if(source_2 && (source_3 + source_4 + source_5 + source_6)){ sqlite_q = sqlite_q + "OR"}
    if(source_3){sqlite_q = sqlite_q + " SOURCE = 3 ";}
    if(source_3 && (source_4 + source_5 + source_6)){ sqlite_q = sqlite_q + "OR"}
    if(source_4){ sqlite_q = sqlite_q + " SOURCE = 4 ";}
    if(source_4 && (source_5 + source_6)){ sqlite_q = sqlite_q + "OR"}
    if(source_5){sqlite_q = sqlite_q + " SOURCE = 5 ";}
    if(source_5 && (source_6)){ sqlite_q = sqlite_q + "OR"}
    if(source_6){sqlite_q = sqlite_q + " SOURCE = 6 ";}
    sqlite_q = sqlite_q + ");"

    var requete = { "message":"Connection request form Web page","data": sqlite_q};

    message = new Paho.MQTT.Message(JSON.stringify(requete));
    message.destinationName = "query";
    client.send(message);
  }


 function onConnect() {
    // Once a connection has been made, make a subscription and send a message.
    console.log("Connected ");
    client.subscribe(subscribeTopic);

    getFilters();
    /* var requete = { "message":"Connection request form Web page","data":"SELECT * FROM data ORDER BY TIME;"}; */

}



$(document).ready(function () {

});
    
function mySubmit() {
        console.log("in submit");
        var topic = $("#topic").val();
        var message = $("#message").val();
        console.log("topic:"+topic+" message:"+message);
        var requete = { "message":message,"data":""};
        var message1 = new Paho.MQTT.Message(JSON.stringify(requete));
        message1.destinationName = topic;
        client.send(message1);
}

function myClear() {
        console.log("in clear");
        liste = "";
        $("#textarea1").text(liste);
}

getdate_from();
getdate_to();
